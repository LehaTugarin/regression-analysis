import math as m
x = X
y = y

X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.3, random_state=0)
print('Размер обучающей выборки = ', len(X_train))
print('Размер тестовой выборки = ', len(X_test))

h = (np.max(x) - np.min(x))/100

X_tr = {1:X_train, 2:(1/X_train), 3:(np.log(X_train)), 4:X_train, 5:X_train, 6:X_train, 7:np.log(X_train)}
X_ts = {1:X_test, 2:(1/X_test), 3:(np.log(X_test)), 4:X_test, 5:X_test, 6:X_test, 7:np.log(X_test)}
Y_tr = {1:y_train, 2:y_train, 3:y_train, 4:np.log(abs(y_train)), 5:np.log(abs(y_train)), 6:np.log(abs(y_train)), 7:np.log(abs(y_train))}
Y_ts = {1:y_test, 2:y_test, 3:y_test, 4:np.log(abs(y_test)), 5:np.log(abs(y_test)), 6:np.log(abs(y_test)), 7:np.log(abs(y_test))}

models = ["y=b*x+a", "y=a+b/x", "y=b*ln(x)+a", "y=a*exp(b*x)", "y=exp(a+b*x)", "y=a*b^(x)", "y=a*x^(b)"]

ertr = []
erts = []
ertr1 = []
erts1 = []

sr = LinearRegression()

xfit = np.array([np.min(x) + i * h for i in range(101)])

for i in range(1,8):

    slr = sr.fit(X_tr[i], Y_tr[i])
    A = {1:np.round(slr.intercept_, 4), 2:np.round(slr.intercept_, 4), 3:np.round(slr.intercept_, 4), 4:np.exp(np.round(slr.intercept_, 4)), 5:np.round(slr.intercept_, 4), 6:np.exp(np.round(slr.intercept_, 4)), 7:np.exp(np.round(slr.intercept_, 4))}
    B = {1:np.round(slr.coef_, 4), 2:np.round(slr.coef_, 4), 3:np.round(slr.coef_, 4), 4:np.round(slr.coef_, 4), 5:np.round(slr.coef_, 4), 6:np.exp(np.round(slr.coef_, 4)), 7:np.round(slr.coef_, 4)}


    r_sq1 = round(slr.score(X_tr[i], Y_tr[i]), 4)
    r_sq2 = round(slr.score(X_ts[i], Y_ts[i]), 4)
    corr1 = round(m.sqrt(r_sq1), 4)
    corr2 = round(m.sqrt(r_sq2), 4)
    a = A[i]
    b = B[i]

    ypr11 = {1:(a + b * X_train), 2:(a + b / X_train), 3:(b * np.log(abs(X_train)) + a), 4:(a * np.exp(b * X_train)), 5:(np.exp(a + b * X_train)), 6:(a * (b ** X_train)), 7:(a * (X_train ** b)) }
    ypr21 = {1:(a + b * X_test), 2:(a + b / X_test), 3:(b * np.log(abs(X_test)) + a), 4:(a * np.exp(b * X_test)), 5:(np.exp(a + b * X_test)), 6:(a * (b ** X_test)), 7:(a * (X_test ** b)) }

    ypr1 = ypr11[i]
    ypr2 = ypr21[i]

    delta12 = (y_train - ypr1)**2
    delta22 = (y_test - ypr2)**2

    er3 = sum(delta12) / len(X_tr[i])
    er4 = sum(delta22) / len(X_ts[i])
    ertr1.append(er3[0])
    erts1.append(er4[0])

    print( i )
    print('R^2 train = ', r_sq1, ' test = ', r_sq2)
    print('Коэффициент корреляции train = ', corr1)
    print('Коэффициент корреляции test = ', corr2)
    print('b = ', b)
    print('a = ', a)
    print('MSE train', round(np.mean(delta12), 4), ' test', round(np.mean(delta22), 4))
    print('Эмпирический риск train', er3)
    print('Эмпирический риск test', er4)
