import math as m

x = df[['sqft_living']].values
y = df[['price']].values

X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.3, random_state=0)

# y=b*x+a
x_train = X_train
x_test = X_test

model = LinearRegression()
slr = model.fit(x_train, y_train)

a = slr.intercept_[0]
b = slr.coef_[0][0]

r_square_train = slr.score(x_train, y_train)
r_square_test= slr.score(x_test, y_test)
corr_train = m.sqrt(r_square_train)
corr_test = m.sqrt(r_square_test)


y_predict_train = a + b * x_train
y_predict_test = a + b * x_test

empiric_risk_train = sum((y_train - y_predict_train)**2) / len(x_train)
empiric_risk_test = sum((y_test - y_predict_test)**2) / len(x_test)


print('R^2 train = {} test = {}'.format(slr.score(x_train, y_train), r_sq2))
print('Коэффициент корреляции train = ', corr_train)
print('Коэффициент корреляции test = ', corr_test)
print('b = ', b)
print('a = ', a)
print('MSE train= {} test={}'.format(round(np.mean((y_train - y_predict_train)**2), 4), round(np.mean((y_test - y_predict_test)**2), 4)))
print('Эмпирический риск train', empiric_risk_train)
print('Эмпирический риск test', empiric_risk_test)
